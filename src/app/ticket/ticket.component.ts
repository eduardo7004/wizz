import { Router } from '@angular/router';
import { ACTIONS } from '../shared/actions';
import { FlightService } from '../shared/flight.service';
import { Fare, Flight, SelectedTicket, Station, Ticket } from '../shared/interfaces';
import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TicketComponent implements OnInit {
  flight: Flight = <Flight>{};
  stations: Station[];
  tickets: Ticket[] = [];
  selectedOrigin: SelectedTicket = <SelectedTicket>{};
  selectedReturn: SelectedTicket = <SelectedTicket>{};
  showReturn = false;
  totalCost = 0;
  showDialog = false;
  returnSelectModal: Date;
  returnMinDate: Date;
  // this.subscription =

  constructor(private store: Store<any>,
    private flightService: FlightService,
    private router: Router) { }


  ngOnInit() {
    this.store.select('flight').subscribe(flight => {
      const body = <Flight>flight;
      if (!body.destinationCountry) {
        this.router.navigate(['./index']);
        return;
      }
      this.flight = body;
      this.returnMinDate = new Date(flight.derpatureDate.getFullYear(), flight.derpatureDate.getMonth(), flight.derpatureDate.getDate() + 1)
      console.log(body)
      this.flightService.searchOriginTicketByFlight(body).subscribe(tickets => {
        console.log(tickets)
        tickets = tickets.map(ticke => {
          ticke.fares.map(fare => {
            fare.selected = false;
            return fare;
          })
          return ticke
        })
        this.tickets = tickets;
      })
    });

    this.store.select('originTicket').subscribe(selectedOrigin => {
      const body = <SelectedTicket>selectedOrigin;
      this.selectedOrigin = body;
      if (body.selectedFare) {
        this.totalCost = body.selectedFare.price;
        if (this.selectedReturn.selectedFare) {
          this.totalCost = body.selectedFare.price + this.selectedReturn.selectedFare.price;
        }
      }
    });

    this.store.select('returnTicket').subscribe(selectedOrigin => {
      const body = <SelectedTicket>selectedOrigin;
      this.selectedReturn = body;
      if (body.selectedFare) {
        this.showReturn = true;

        if (this.selectedOrigin.selectedFare) {
          this.totalCost = body.selectedFare.price + this.selectedOrigin.selectedFare.price;
        }
        // this.totalCost += body.selectedFare.price;
      }

    });
  }

  checkClass(fare) {
    console.log(fare)
    return '';
  }
  checkSucess(fare: Fare) {
    return fare.bundle === 'basic';
  }
  checkWarning(fare: Fare) {
    return fare.bundle === 'standard';
  }
  checkDanger(fare: Fare) {
    return fare.bundle === 'plus';
  }

  selectFare(fare: Fare, ticket: Ticket, i: number) {
    this.tickets.forEach(ticketIterator => {
      ticketIterator.fares.forEach(fareIterator => {
        fareIterator.selected = false;
      })
    })
    fare.selected = !fare.selected;
    const result: any = Object.assign({}, ticket);
    result.selectedFare = ticket.fares[i];

    const typeResult: SelectedTicket = result;
    console.log(typeResult)
    let actionType = '';
    if (this.showReturn) {
      actionType = ACTIONS.SELECT_RETURN_FLIGHT
    } else {
      actionType = ACTIONS.SELECT_ORIGIN_FLIGHT
    }
    const event = {
      type: actionType,
      payload: typeResult
    }

    this.store.dispatch(event)
  }

  unselectFare(fare: Fare, ticket: Ticket, i: number) {
    fare.selected = !fare.selected;
    let actionType = '';
    if (this.showReturn) {
      actionType = ACTIONS.UNSELECT_RETURN_FLIGHT
    } else {
      actionType = ACTIONS.UNSELECT_ORIGIN_FLIGHT
    }
    const event = {
      type: actionType
    }
    this.store.dispatch(event)
  }

  selectReturnFlight() {
    window.scrollTo(0, 50);
    this.showReturn = true;
    this.flightService.searchReturnTicketByFlight(this.flight).subscribe(tickets => {
      console.log(tickets)
      tickets = tickets.map(ticke => {
        ticke.fares.map(fare => {
          fare.selected = false;
          return fare;
        })
        return ticke
      })
      this.tickets = tickets;
    })
  }

  goToSummary() {
    this.router.navigate(['./summary']);
  }

  showModal() {
    this.showDialog = true;
  }

  addReturnFlight() {
    console.log(this.returnSelectModal)
    this.flight.oneWay = false;
    this.flight.returnDate = this.returnSelectModal;

    const event = {
      type: ACTIONS.ADD_FLIGHT,
      payload: this.flight
    }
    this.store.dispatch(event)
    this.showDialog = false;
    this.selectReturnFlight();
  }

}
