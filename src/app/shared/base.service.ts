import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';

@Injectable()
export class BaseService {

  constructor() { }

  protected handleError(error: Response | any) {
    const body = error.json();
    return Observable.throw(body);
  }

  protected handleErrorFull(error: Response | any) {
    return Observable.throw(error);  }

  protected extractDataList(res: Response) {
    const body = res.json();    return body.items || [];
  }

  protected extractDataObject(res: Response) {
    const body = res.json();
    return body || {};
  }

  protected addAuthHeader(authToken = '') {
    const headers: Headers = new Headers();
    headers.append('firebase_token', authToken);
    return new RequestOptions({headers: headers, withCredentials: true});
  }
}
