import { Flight, Station, Ticket } from './interfaces';
import { Observable } from 'rxjs/Rx';
import { BaseService } from './base.service';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class FlightService extends BaseService {

  stationsUrl = 'http://78.24.185.27:8570/asset/stations';

  constructor(private http: Http) {
    super()
  }

  getAllStations(): Observable<Station[]> {
    return this.http.get(this.stationsUrl)
      .map(this.extractDataObject)
  }

  searchOriginTicketByFlight(flight: Flight): Observable<Ticket[]> {
    const date = flight.derpatureDate.toISOString().slice(0, 10);
    let ticketUrl = `http://78.24.185.27:8570/search?departureStation=${flight.originCountry.iata}`
    ticketUrl += `&arrivalStation=${flight.destinationCountry.iata}&date=${date}`

    return this.http.get(ticketUrl)
      .map(this.extractDataObject)
  }

  searchReturnTicketByFlight(flight: Flight): Observable<Ticket[]> {
    const date = flight.returnDate.toISOString().slice(0, 10);
    let ticketUrl = `http://78.24.185.27:8570/search?departureStation=${flight.destinationCountry.iata}`
    ticketUrl += `&arrivalStation=${flight.originCountry.iata}&date=${date}`

    return this.http.get(ticketUrl)
      .map(this.extractDataObject)
  }

}
