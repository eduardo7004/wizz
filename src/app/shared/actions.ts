import { Flight, Ticket } from './interfaces';
import { Action } from '@ngrx/store';

export const ACTIONS = {
    ADD_FLIGHT: 'ADD_FLIGHT',
    SELECT_ORIGIN_FLIGHT: 'SELECT_ORIGIN_FLIGHT',
    SELECT_RETURN_FLIGHT: 'SELECT_RETURN_FLIGHT',
    UNSELECT_ORIGIN_FLIGHT: 'UNSELECT_ORIGIN_FLIGHT',
    UNSELECT_RETURN_FLIGHT: 'UNSELECT_RETURN_FLIGHT',
    RESTORE_INITIAL_STATE: 'RESTORE_INITIAL_STATE',
}

export class AddFlightAction implements Action {
    readonly type = ACTIONS.ADD_FLIGHT;

    constructor(public payload: Flight) { }
}

export class SelectOriginTicket implements Action {
    readonly type = ACTIONS.SELECT_ORIGIN_FLIGHT;

    constructor(public payload: Ticket) { }
}

export class SelectReturnTicket implements Action {
    readonly type = ACTIONS.SELECT_RETURN_FLIGHT;

    constructor(public payload: Ticket) { }
}

export class UnSelectReturnTicket implements Action {
    readonly type = ACTIONS.SELECT_RETURN_FLIGHT;

    constructor(public payload: Ticket) { }
}


export class UnSelectOriginTicket implements Action {
    readonly type = ACTIONS.SELECT_RETURN_FLIGHT;

    constructor(public payload: Ticket) { }
}

export class RestoreInitalState implements Action {
    readonly type = ACTIONS.RESTORE_INITIAL_STATE;

    constructor(public payload: Ticket) { }
}




export type Actions
    = AddFlightAction
    | SelectOriginTicket
    | SelectReturnTicket
    | UnSelectReturnTicket
    | RestoreInitalState
