export interface Station {
    iata: string,
    latitude: number,
    longitude: number,
    shortName: string,
    connections: Connection[]
}

export interface Connection {
    iata: string,
    operationStartDate: Date,
    rescueEndDate: Date
}

export interface Flight {
    derpatureDate: Date,
    destinationCountry: Station,
    oneWay: boolean,
    returnDate: Date,
    originCountry: Station;
}


export interface Ticket {
    carrierCode: string,
    flightNumber: string,
    remainingTickets: number,
    departure: Date,
    arrival: Date,
    selected: boolean,
    fares: Fare[]
}

export interface Fare {
    fareSellKey: string,
    price: number,
    selected: boolean,
    bundle: string
}

export interface SelectedTicket extends Ticket {
    selectedFare: Fare;
}
