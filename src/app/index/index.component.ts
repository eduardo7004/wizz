import { ACTIONS } from '../shared/actions';
import { Router } from '@angular/router';

import { Station } from '../shared/interfaces';
import { FlightService } from '../shared/flight.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';

declare var $: any;

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  form: FormGroup;
  stations: Station[];
  countries: string[];
  country = '';
  originSelectedObj: Station;
  filteredOrigins: string[];
  destinations: string[];
  filteredDestinations: string[];
  today = new Date();
  derpatureMaxDate;
  checked: boolean;
  returnMinDate = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate() + 1)
  returnDisalbed = false;
  errorMsg = '';



  constructor(private formBuilder: FormBuilder,
    private flightService: FlightService,
    private router: Router,
    private store: Store<any>) { }

  ngOnInit() {
    const init = <any>{};
    const s = localStorage.getItem('origin');
    const origin = JSON.parse(s);

    if (origin) {
      init.origin = origin;

      const destinationStr = localStorage.getItem('destinationSelected');
      const destinationSelected = JSON.parse(destinationStr);
      if (destinationSelected) {
        init.destinationSelected = destinationSelected;
      }
    }


    this.form = this.initForm(init);
    this.flightService.getAllStations().subscribe(stations => {
      this.stations = stations;
      this.countries = this.stations.map(station => station.shortName)
      this.filteredOrigins = Object.assign({}, this.countries);
      if (origin) {
        const desti = origin.connections.map(conn => conn.iata);
        this.destinations = this.findConnectionsByIata(desti);
      }
    })

    const resetEvent = {
      type: ACTIONS.RESTORE_INITIAL_STATE
    }

    this.store.dispatch(resetEvent);
  }

  initForm(init): FormGroup {
    const date = new Date();
    const requiredDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1);
    let orignStr = '';
    if (init.origin) {
      orignStr = init.origin.shortName;
    }
    return this
      .formBuilder
      .group({
        originCountry: [orignStr, Validators.required],
        destinationCountry: [init.destinationSelected || '', Validators.required],
        derpatureDate: [date, Validators.required],
        returnDate: [requiredDate, Validators.required]
      });

  }

  search(event) {

  }

  filterOriginSingle(event) {
    const query = event.query;
    this.filteredOrigins = this.filterOrigin(query, this.countries);
  }

  filterOrigin(query, countries: any[]): any[] {
    const filtered: any[] = [];
    for (let i = 0; i < countries.length; i++) {
      const country = countries[i];
      if (country.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(country);
      }
    }
    return filtered;
  }

  originSelected() {
    const country = this.form.controls.originCountry.value;
    this.originSelectedObj = this.stations.find(count => count.shortName === country)
    const dest = this.originSelectedObj.connections.map(conn => conn.iata);
    this.destinations = this.findConnectionsByIata(dest);


    const def = this.destinations[0] || '';
    this.form.controls.destinationCountry.setValue(def);

    localStorage.setItem('origin', JSON.stringify(this.originSelectedObj));
    localStorage.setItem('destinations', JSON.stringify(this.destinations));
  }

  filterDestinationSingle(event) {
    const query = event.query;
    this.filteredDestinations = this.filterDestination(query, this.destinations);
  }

  filterDestination(query, countries: any[]): any[] {
    const filtered: any[] = [];
    for (let i = 0; i < countries.length; i++) {
      const country = countries[i];
      if (country.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(country);
      }
    }
    return filtered;
  }


  findConnectionsByIata(iatas) {
    const countryNames = [];
    this.stations.forEach(station => {
      const c = iatas.find(iata => iata === station.iata);
      if (c) {
        countryNames.push(station.shortName);
      }
    })
    return countryNames;
  }

  departureSelected() {
    const departure = this.form.controls.derpatureDate.value;
    const requiredDate = new Date(departure.getFullYear(), departure.getMonth(), departure.getDate() + 1)
    this.returnMinDate = requiredDate;
    if (this.returnMinDate > this.form.controls.returnDate.value && !this.returnDisalbed) {
      this.form.controls.returnDate.setValue(requiredDate);
    }
  }

  returnSelected() {

  }

  onDestinationChange(event) {
    console.log(event)
    localStorage.setItem('destinationSelected', JSON.stringify(event));
  }
  switchChanged(event) {
    const checked = event.checked;
    this.returnDisalbed = checked;
    if (checked) {
      this.form.controls.returnDate.setValidators(null);
      this.form.controls.returnDate.setValue('');
      this.form.controls.returnDate.disable();

    } else {
      const departure = this.form.controls.derpatureDate.value;
      const requiredDate = new Date(departure.getFullYear(), departure.getMonth(), departure.getDate() + 1)
      this.form.controls.returnDate.setValue(requiredDate);
      this.form.controls.returnDate.setValidators(Validators.required);
      this.form.controls.returnDate.enable();
    }
    this.form.controls.returnDate.updateValueAndValidity();
  }

  submitForm() {
    const result = this.form.value;
    const dest = this.stations.find(station => station.shortName === result.destinationCountry);
    result.destinationCountry = dest;
    const ori = this.stations.find(station => station.shortName === result.originCountry);
    console.log(ori)
    if (!ori) {
      this.errorMsg = 'Invalid Origin Selected';
      return;
    }

    result.originCountry = ori;
    result.oneWay = this.returnDisalbed;
    const event = {
      type: ACTIONS.ADD_FLIGHT,
      payload: result
    }

    this.store.dispatch(event)
    this.router.navigate(['./ticket']);

  }

}
