import { Flight, SelectedTicket, Ticket } from '../shared/interfaces';
import { ACTIONS } from '../shared/actions';
import { Action } from '@ngrx/store';

export function originTicketReducer(state: SelectedTicket = <SelectedTicket>{}, action) {
    switch (action.type) {
        case ACTIONS.SELECT_ORIGIN_FLIGHT:
            return Object.assign({}, action.payload);

        case ACTIONS.UNSELECT_ORIGIN_FLIGHT:
            return <SelectedTicket>{};
        case ACTIONS.RESTORE_INITIAL_STATE:
            return <SelectedTicket>{};

        default:
            return state;
    }
}

export function returnTicketReducer(state: SelectedTicket = <SelectedTicket>{}, action) {
    switch (action.type) {
        case ACTIONS.SELECT_RETURN_FLIGHT:
            return Object.assign({}, action.payload);
        case ACTIONS.UNSELECT_RETURN_FLIGHT:
            return <SelectedTicket>{};
        case ACTIONS.RESTORE_INITIAL_STATE:
            return <SelectedTicket>{};

        default:
            return state;
    }
}

