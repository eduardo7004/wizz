import { Flight } from '../shared/interfaces';
import { ACTIONS } from '../shared/actions';
import { Action } from '@ngrx/store';

export function flightReducer(state: Flight = <Flight>{}, action) {
    switch (action.type) {
        case ACTIONS.ADD_FLIGHT:
            return Object.assign({}, action.payload);

        case ACTIONS.ADD_FLIGHT:
            return Object.assign({}, action.payload);

        case ACTIONS.RESTORE_INITIAL_STATE:
            return <Flight>{};

        default:
            return state;
    }
}
