import { originTicketReducer, returnTicketReducer } from './reducers/ticket.reducer';
import { flightReducer } from './reducers/flight.reducer';
import { FlightService } from './shared/flight.service';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AutoCompleteModule } from 'primeng/primeng';
import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { CalendarModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { InputSwitchModule } from 'primeng/primeng';
import { TicketComponent } from './ticket/ticket.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { SummaryComponent } from './summary/summary.component';
import {DialogModule} from 'primeng/primeng';

declare var $: any;
const appReducers: any = {
  flight: flightReducer,
  originTicket: originTicketReducer,
  returnTicket: returnTicketReducer
};

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    TicketComponent,
    SummaryComponent
  ],
  imports: [
    StoreModule.forRoot(appReducers),
    StoreDevtoolsModule.instrument({ maxAge: 50 }),
    BrowserModule,
    BrowserAnimationsModule,
    AutoCompleteModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    HttpModule,
    CalendarModule,
    DropdownModule,
    InputSwitchModule,
    DialogModule,
    AppRoutingModule
  ],
  providers: [FlightService],
  bootstrap: [AppComponent]
})
export class AppModule { }
