import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { Fare, Flight, SelectedTicket, Station, Ticket } from '../shared/interfaces';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  selectedOrigin: SelectedTicket = <SelectedTicket>{};
  selectedReturn: SelectedTicket = <SelectedTicket>{};
  flight: Flight = <Flight>{};
  totalCost = 0;

  constructor(private store: Store<any>, private router: Router) { }

  ngOnInit() {
    this.store.select('originTicket').subscribe(selectedOrigin => {
      const body = <SelectedTicket>selectedOrigin;
      this.selectedOrigin = body;
      console.log(body)
      if (body.selectedFare) {
        this.totalCost += body.selectedFare.price;
      }

    });

    this.store.select('returnTicket').subscribe(selectedOrigin => {
      const body = <SelectedTicket>selectedOrigin;
      this.selectedReturn = body;
      console.log(body)
      if (body.selectedFare) {
        this.totalCost += body.selectedFare.price;
      }
    });

    this.store.select('flight').subscribe(flight => {
      if (!flight.destinationCountry) {
        this.router.navigate(['./index']);
      }
      this.flight = flight;
    })

  }

}
