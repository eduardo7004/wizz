import { SummaryComponent } from './summary/summary.component';
import { TicketComponent } from './ticket/ticket.component';
import { IndexComponent } from './index/index.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const appRoutes: Routes = [
    { path: 'index', component: IndexComponent },
    { path: 'ticket', component: TicketComponent },
    { path: 'summary', component: SummaryComponent },
    { path: '', redirectTo: '/index', pathMatch: 'full' },

];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            { enableTracing: true } // <-- debugging purposes only
        )
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule { }
