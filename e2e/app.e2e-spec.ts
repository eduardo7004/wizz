import { WizzPage } from './app.po';

describe('wizz App', () => {
  let page: WizzPage;

  beforeEach(() => {
    page = new WizzPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
