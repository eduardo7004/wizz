# Wizz Air - App
# Live version running at [Amazon](http://wizz-air-edu.s3-website.eu-central-1.amazonaws.com).

This project was generated using angular CLI,  To run it: 

### First download the repo 
### Install the dependencies with npm install
### Run it by using ng serve

## Goal - The task given to me

https://docs.google.com/document/d/1Psd-VchhT8JBOYtvHP2NwWful-FcG4IddX0qCbffJXI/edit

## Technologies chosen and why

So at first when I saw this assigment the first technology that came to my mind was angular. First because I am more experienced seccond because I knew how to do all goals required with it. Also with the help of the CLI it helps to keep the code organized and understable.

I also used NGRX for managing the state of my application.

For styling I used bootstrap v3.

## Idea of Design and UX

The First screen is a pretty standard form, by using calendar components it makes easier for the user to select the date he wants and avoid typing problems. I decided to use an autocomplete for the first one because there are a lot of origins to use a dropdown

After the user selects the origin the destination is filled and ordered alphabetically. I also used the swicth pattern for one-way/return flights.

The form is only submitable if the form has all filled and then it is done a verifcation on the origin if it is not valid a message is shown.

For the seccond screen I used kind of a card pattern to separate the fares that are grouped by flights. I also used different colors for each bundle so the user can see the difference and the pattern of the flights.

The user can see the details of what he has selected so far on the right so as he progesses and changes it is clear to him what he has selected so far.

Also he can select a return flight if he hasnt done it already.

After he selects both fares he can see the summary which is pretty simple in itself.

The app also doesnt allow to acesss other screens that not the first directly 


## Bumps on the road and if I have more time

 - [ ] Make it responsive.

 - [ ] improve overall layout and UX of the applcation.

 - [ ] Make possible to change everything about the flight on the seccond screen.
